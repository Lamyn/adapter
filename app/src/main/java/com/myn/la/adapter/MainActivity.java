package com.myn.la.adapter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private static final String KEY_IMAGE = "image";
    private static final String KEY_DECRIPTION = "decription";
    private static final String KEY_TITLE = "title";


    String[] from = {KEY_IMAGE,KEY_TITLE,KEY_DECRIPTION};
    int[] to = {R.id.iv_pic,R.id.tv_title,R.id.tv_decription};

    ListView listView;
    SimpleAdapter adapter;
    ArrayList<HashMap<String, Object>> list;
    RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list);
        ratingBar = (RatingBar) findViewById(R.id.tv_ratingBar);
        list = new ArrayList<>();
        // можно еще закинуть в цикл
        HashMap<String, Object> hm = new HashMap<>();
        hm.put(KEY_IMAGE, R.drawable.cat);
        hm.put(KEY_TITLE, "Android: ListView");
        hm.put(KEY_DECRIPTION, "ArrayAdapter, SimpleAdapter . . . . .");

        for(int i = 0; i < 10; i++){
            list.add(hm);
        }
        adapter = new SimpleAdapter(this, list, R.layout.list_item, from, to);

        list.get(4).put(KEY_TITLE, "Привет "); // таким способом можно заменить
        list.get(4).put(KEY_DECRIPTION, "я кот ^0^ ");
        listView.setAdapter(adapter);
    }
}
